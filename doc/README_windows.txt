banglacoin Core 0.14.2
=====================

Intro
-----
banglacoin is a free open source peer-to-peer electronic cash system that is
completely decentralized, without the need for a central server or trusted
parties.  Users hold the crypto keys to their own money and transact directly
with each other, with the help of a P2P network to check for double-spending.


Setup
-----
Unpack the files into a directory and run banglacoin-qt.exe.

banglacoin Core is the original banglacoin client and it builds the backbone of the network.
However, it downloads and stores the entire history of banglacoin transactions;
depending on the speed of your computer and network connection, the synchronization
process can take anywhere from a few hours to a day or more.

See the banglacoin wiki at:
  https://banglacoin.info/
for more help and information.
